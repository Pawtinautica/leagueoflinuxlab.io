# Hextech Mayhem: A League of Legends Story

* 🖥️ Full gameplay functionality via Steam Proton.
* 🖥️ Available on [Steam](https://store.steampowered.com/app/1651960/Hextech_Mayhem_A_League_of_Legends_Story/) and [Epic Games](https://www.epicgames.com/store/en-US/p/hextech-mayhem-a-league-of-legends-story).
* 🔗 [ProtonDB](https://www.protondb.com/app/1651960), [main site](https://hextechmayhem.com/en-us/).
* Elsewhere on reddit at r/hextechmayhem.