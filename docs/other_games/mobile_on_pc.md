# Mobile on PC

📱➡️🖥️ For all of Riot's mobile games it is possible to play the Android versions on desktop. There are two main methods:

* Connect your Android phone to your PC and run [scrcpy](https://github.com/Genymobile/scrcpy) to display and control your phone from your PC
* Use an Android emulator such as [Anbox](https://github.com/anbox/anbox) or [Android-x86](https://www.android-x86.org/)
 * ⚠️ Use at your own risk and discretion; Riot has not stated whether or not emulators are considered a bannable offense