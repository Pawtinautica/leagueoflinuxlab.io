# 📈 How to Optimise League of Legends

As many of us already know, the League of Legends client is [not known for it's stability](https://youtu.be/WUff32Y8rwo), even on Windows... therefore you shouldn't expect a buttery smooth client experience on Linux either. The game itself however runs at near-native on most properly configured machines. There are some tweaks that can help make the experience a bit nicer both in the client and in the game.

## Optimal Game and Client Settings

* **❗High impact:** enabling the setting in client settings `Enable low spec mode` disables a lot of the lengthy and resource-taxing animations inside the client, particularly related to champion select
* For machines with low RAM: enable the setting inside the client settings `Close client during game`. This will make post-game lobbies load slower, but will make in-game performance better. If you are *not* experiencing RAM limitations then it's best to not enable this option
* Change in-game video settings:
    - `Window mode` == `Borderless`
    - `Graphics` == Depends on your machines specs. If you're not sure, test different settings and tweak as you play. In general the most intense settings that should be reduced first are `Shadow Quality` and `Environment Quality`
    - `Character Inking` == `False`
    - `Frame Rate Cap` == If you have a weaker machine set this to your monitors refresh rate, this eliminates wasted CPU cycles by not rendering frames that your monitor can't display. If you have a powerful machine set this to double your monitors refresh rate (eg. 60hz monitor == ``120 FPS`` cap), this is a decent middle ground between curbing wasted CPU cycles and not giving up too many input frames
    - `Anti-Aliasing` == `False`
    - `Wait for Vertical Sync` == `False`

## System and Lutris Optimizations

* Adding `D3D10` and `D3D11` native `.dll` overrides for the Wine prefix can increases performance and fluidity in the client. Thanks to /u/wiryfuture for [discovering this](https://web.archive.org/web/20230317043141/https://old.reddit.com/r/leagueoflinux/comments/qyjv45/decreased_client_lag_no_more_low_spec_mode/). To do so in Lutris:
    1. Select League of Legends in Lutris
    2. Select the 🔺triangle next to Wine symbol
    3. Select `Wine configuration`
    4. In the new Wine dialogue select `Libraries`
    5. Add a new override for the library `d3d10` and set it to `Native (Windows)` using the `Edit...` button
    6. Add a new override for the library `d3d11` and set it to `Native (Windows)` using the `Edit...` button, then `OK`
* Download and install [Feral Gamemode](https://github.com/FeralInteractive/gamemode). Gamemode is is a daemon/lib combo for Linux that allows games to request a set of optimizations be temporarily applied to the host OS and/or a game process. Make sure to the version you install includes the `i386` binaries, this is _not_ the case with the version found in the Ubuntu repos; you must build them yourself. Once you have installed Gamemode you need to enable the option in Lutris:
    1. Right click League of Legends
    2. Select `Configure`
    3. Select `System Options`
    4. Enable `Enable Feral Gamemode` then `Save` 
* Enable `Esync` in Lutris. Esync removes wineserver overhead for synchronization objects. This can increase performance for some games, especially ones that rely heavily on the CPU. For more information see the [Lutris wiki](https://github.com/lutris/docs/blob/master/HowToEsync.md):
    1. Right Click League of Legends
    2. Select `Runner options`
    3. Enable `Enable Esync` then `Save`
* If your kernel support it ([example](https://packages.archlinux.org/packages/linux-zen)) you may want to enable `Fsync` which is a better version of `Esync`. If your kernel does not support `Fsync` it will fall back to `Esync`:
    1. Right Click League of Legends
    2. Select `Runner options`
    3. Enable `Enable Fsync` then `Save`

## wine-lol PKGBUILD

Maintained by /u/m-reimer, the [wine-lol](https://aur.archlinux.org/packages/wine-lol) (and [wine-lol-bin](https://aur.archlinux.org/packages/wine-lol-bin)) PKGBUILDs are a convenient way for Arch (derivative) users to build their own "LoL capable" Wine version or to keep one updated using the AUR helper of your choice. Both PKGBUILDs are based on "Lutris-GE-LoL" which is maintained by GloriousEggroll. 

The resulting Wine setup can be used with whatever way of installing LoL you like best. All you need to know here is that your "wine" executable, that should be used, can be found at `/opt/wine-lol/bin/wine`.

If you want to use your "self compiled" version in Lutris, then this is possible, too, as the new PKGBUILDs build with 64 bit support. To do this, click the box `Show advance options` in the Lutris configure dialogue and then in the `Runner options` tab chose:

* `Wine version` == `Custom (select executable below)`
* `Custom Wine executable` == `/opt/wine-lol/bin/wine`

The PKGBUILD is "just another way" to install (or manually compile) the work done by GloriousEggroll. If you are not using any Arch derivative, then you can get the binaries directly from the [wine-ge-custom releases page](https://github.com/GloriousEggroll/wine-ge-custom/releases?q=-LoL).
